//
//  ViewController.swift
//  HikVisionExtDemoSwift
//
//  Created by Dan on 26.11.2021.
//

import UIKit

class ViewController: UIViewController, HveBrowserDelegate {
    var browser: HveBrowser!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        browser = HveBrowser(type: "_psia._tcp")
        browser.delegates.add(self)
        browser.start()
    }
    
    func hveBrowser(_ sender: HveBrowser!, endpointFound endpoint: HveEndpoint!) {
        sender.stop()
        
        let client = HveClient(endpoint: endpoint, username: "admin", password: "qwerty-123")!
//        activateV1(client: client)
//        wirelessUpdate(client: client)
//        ipAddressUpdate(client: client)
//        deviceInfoGet(client: client)
//        streamingChannelUpdate(client: client)
        timeUpdateV1(client: client)
    }
    
    func activateV1(client: HveClient) {
        let activateInfo = HveActivateInfo()
        activateInfo.password = "qwerty-123"
        
        client.activateV1Async(activateInfo) { responseStatus, error in
            if responseStatus == nil {
                print("Error - \(error!.localizedDescription)")
            } else {
                print("Activated")
            }
        }
    }
    
    func wirelessUpdate(client: HveClient) {
        let wpa = HveWPA()
        wpa.algorithmType = "TKIP"
        wpa.sharedKey = "12345678"
        wpa.wpaKeyLength = wpa.sharedKey.count
        
        let wirelessSecurity = HveWirelessSecurity()
        wirelessSecurity.securityMode = "WPA2-personal"
        wirelessSecurity.wpa = wpa
        
        let wireless = HveWireless()
        wireless.enabled = true
        wireless.ssid = "My network"
        wireless.wirelessSecurity = wirelessSecurity
        
        client.wirelessUpdateAsync("2", wireless: wireless) { responseStatus, error in
            if responseStatus == nil {
                print("Error - \(error!.localizedDescription)")
            } else {
                print("Connecting to \(wireless.ssid!)")
            }
        }
    }
    
    func ipAddressUpdate(client: HveClient) {
        let ipv6Mode = HveIPv6Mode()
        ipv6Mode.ipv6AddressingType = "dhcp"
        
        let ipAddress = HveIPAddress()
        ipAddress.ipVersion = "dual"
        ipAddress.addressingType = "dynamic"
        ipAddress.ipv6Mode = ipv6Mode
        
        client.ipAddressUpdateAsync("1", ipAddress: ipAddress) { responseStatus, error in
            if responseStatus == nil {
                print("Error - \(error!.localizedDescription)")
            } else {
                client.rebootAsync(nil)
            }
        }
    }
    
    func deviceInfoGet(client: HveClient) {
        client.deviceInfoGetAsync { deviceInfo, error in
            if deviceInfo == nil {
                print("Error - \(error!.localizedDescription)")
            } else {
                print("Model - \(deviceInfo!.model!)")
            }
        }
    }
    
    func streamingChannelUpdate(client: HveClient) {
        let video = HveVideo()
        video.videoCodecType = "H.264"
        video.videoResolutionWidth = 1920
        video.videoResolutionHeight = 1080
        video.videoQualityControlType = "VBR"
        video.maxFrameRate = 2500
        
        let audio = HveAudio()
        audio.enabled = true
        audio.audioCompressionType = "G.726"
        
        let streamingChannel = HveStreamingChannel()
        streamingChannel.video = video
        streamingChannel.audio = audio
        
        client.streamingChannelUpdateAsync("101", streamingChannel: streamingChannel) { responseStatus, error in
            if responseStatus == nil {
                print("Error - \(error!.localizedDescription)")
            } else {
                print("Codec - \(video.videoCodecType!)")
            }
        }
    }
    
    func timeUpdateV1(client: HveClient) {
        client.timeUpdateV1Async { responseStatus, error in
            if responseStatus == nil {
                print("Error - \(error!.localizedDescription)")
            } else {
                print("Time updated")
            }
        }
    }
}
